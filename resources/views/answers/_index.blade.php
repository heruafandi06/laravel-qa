@if ($answersCount > 0)
    <div class="row mt-4">
        <div class="col-md-12">
            <div class="card">
            <div class="card-body">
                <div class="card-title">
                <h2>{{ $answersCount . ' ' . \Illuminate\Support\Str::plural('Answer', $answersCount) }}</h2>
                </div>
                <hr>
                @include('layouts._message')
                @foreach($answers as $answer)
                <div class="media">
                    @include('shared._vote', [
                        'model' => $answer
                    ])

                    <div class="media-body">
                    {!! $answer->body_html !!} <br>
                        <div class="row mt-3">
                            <div class="col-4">
                                <div class="ml-auto">
                                    @can('update', $answer)
                                    <a href="{{ route('question.answers.edit', [$question->id, $answer->id]) }}" class="btn btn-sm btn-outline-info">Edit</a>
                                    @endcan

                                    @can('delete', $answer)
                                    <form class="form-delete" action="{{ route('question.answers.destroy', [$question->id, $answer->id]) }}" method="post">
                                        @method('delete')
                                        @csrf
                                        <button type="submit" name="button" class="btn btn-sm btn-outline-danger" onclick="return confirm('Are you sure?')">Delete</button>
                                    </form>
                                    @endcan
                                </div>
                            </div>
                            <div class="col-4">

                            </div>
                            <div class="col-4">
                                @include('shared._author', [
                                    'model' => $answer,
                                    'label' => 'Answered'
                                ])
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                @endforeach
            </div>
            </div>
        </div>
    </div>
@endif
